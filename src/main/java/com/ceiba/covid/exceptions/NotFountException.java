package com.ceiba.covid.exceptions;

import com.ceiba.covid.errors.ErrorDto;
import com.ceiba.covid.services.impl.PersonaServicesImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

public class NotFountException extends ApiException {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotFountException.class);

    private static final long serialVersionUID = 1L;

    public NotFountException(String code, String message) {
        super(code, HttpStatus.BAD_REQUEST.value(), message);
        LOGGER.info("code := "+code+" message := "+message+" HttpStatus.BAD_REQUEST.value() := "+HttpStatus.BAD_REQUEST.value());
    }

    public NotFountException(String code, String message, ErrorDto data) {
        super(code, HttpStatus.BAD_REQUEST.value(), message, Arrays.asList(data));
    }

}
