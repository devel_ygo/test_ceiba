package com.ceiba.covid.exceptions;


import com.ceiba.covid.errors.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class ApiException extends Exception {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiException.class);
    private static final long serialVersionUID = 1L;

    private final String code;

    private final int responseCode;

    private final List<ErrorDto> errorList = new ArrayList<>();

    public ApiException(String code, int responseCode, String message) {
        super(message);
        LOGGER.info("code := " + code + " message := " + message + " HttpStatus.BAD_REQUEST.value() := " + HttpStatus.BAD_REQUEST.value());
        this.code = code;
        this.responseCode = responseCode;
    }

    public ApiException(String code, int responseCode, String message, List<ErrorDto> errorList) {
        super(message);
        this.code = code;
        this.responseCode = responseCode;
        this.errorList.addAll(errorList);
    }

    public String getCode() {
        return code;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public List<ErrorDto> getErrorList() {
        return errorList;
    }


}
