package com.ceiba.covid.entities;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "Persona")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "perscedu")
    private String PersCedu;
    @Column(name = "persnomb")
    private String PersNomb;
    @Column(name = "persapel")
    private String PersApel;
    @Column(name = "persfena", nullable = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate PersFena;

    public Persona() {
    }

    public Persona(String persCedu, String persNomb, String persApel, LocalDate persFena) {
        PersCedu = persCedu;
        PersNomb = persNomb;
        PersApel = persApel;
        PersFena = persFena;
    }

    public String getPersCedu() {
        return PersCedu;
    }

    public void setPersCedu(String persCedu) {
        PersCedu = persCedu;
    }

    public String getPersNomb() {
        return PersNomb;
    }

    public void setPersNomb(String persNomb) {
        PersNomb = persNomb;
    }

    public String getPersApel() {
        return PersApel;
    }

    public void setPersApel(String persApel) {
        PersApel = persApel;
    }

    public LocalDate getPersFena() {
        return PersFena;
    }

    public void setPersFena(LocalDate persFena) {
        PersFena = persFena;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "PersCedu='" + PersCedu + '\'' +
                ", PersNomb='" + PersNomb + '\'' +
                ", PersApel='" + PersApel + '\'' +
                ", PersFena=" + PersFena +
                '}';
    }
}
