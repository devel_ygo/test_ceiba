package com.ceiba.covid.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonaRest {

    @JsonProperty("perscedu")
    private String PersCedu;
    @JsonProperty("persnomb")
    private String PersNomb;
    @JsonProperty("persapel")
    private String PersApel;
    @JsonProperty("persfena")
    private LocalDate PersFena;

    public String getPersCedu() {
        return PersCedu;
    }

    public void setPersCedu(String persCedu) {
        PersCedu = persCedu;
    }

    public String getPersNomb() {
        return PersNomb;
    }

    public void setPersNomb(String persNomb) {
        PersNomb = persNomb;
    }

    public String getPersApel() {
        return PersApel;
    }

    public void setPersApel(String persApel) {
        PersApel = persApel;
    }

    public LocalDate getPersFena() {
        return PersFena;
    }

    public void setPersFena(LocalDate persFena) {
        PersFena = persFena;
    }

    @Override
    public String toString() {
        return "PersonaRest{" +
                "PersCedu='" + PersCedu + '\'' +
                ", PersNomb='" + PersNomb + '\'' +
                ", PersApel='" + PersApel + '\'' +
                ", PersFena=" + PersFena +
                '}';
    }
}
