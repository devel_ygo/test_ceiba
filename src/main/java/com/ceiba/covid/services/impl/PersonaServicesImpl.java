package com.ceiba.covid.services.impl;

import com.ceiba.covid.entities.Persona;
import com.ceiba.covid.exceptions.ApiException;
import com.ceiba.covid.exceptions.InternalServerErrorException;
import com.ceiba.covid.exceptions.NotFountException;
import com.ceiba.covid.json.PersonaRest;
import com.ceiba.covid.repositories.PersonaRespository;
import com.ceiba.covid.services.PersonaServices;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonaServicesImpl implements PersonaServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaServicesImpl.class);

    @Autowired
    private PersonaRespository personaRespository;

    private static final ModelMapper modelMapper = new ModelMapper();
    private static final LocalDate ldFechHoy = LocalDate.now();


    @Override
    public List<PersonaRest> getPersonas() throws ApiException {
        final List<Persona> personaList = personaRespository.findAll();
        return personaList.stream().map(service -> modelMapper.map(service, PersonaRest.class))
                .collect(Collectors.toList());
    }

    @Override
    public PersonaRest getPersona(String strPersonaId) throws ApiException {
        LOGGER.info("Ingresa al metodo getPersona con parámetro  " + strPersonaId);
        if (strPersonaId == null) {
            LOGGER.info("Parámetro Nulo");
            throw new NotFountException("ID DE PERSONA NULO", "ID DE PERSONA NULO");
        }
        Optional<Persona> persona = personaRespository.findById(strPersonaId);
        if (!persona.isPresent()) {
            LOGGER.info("PERSONA NO EXISTE");
            new NotFountException("SNOT-404", "PERSONA NO EXISTE");
            return null;
        }
        LOGGER.info("Retornando Persona.");
        return modelMapper.map(persona.get(), PersonaRest.class);
    }

    @Override
    public String crearPersona(PersonaRest personaRest) throws ApiException {
        LOGGER.info("Ingresa al metodo crearPersona con parámetro  " + personaRest.toString());
        LOGGER.info("Fecha actual ldDate := " + ldFechHoy);
        if (personaRespository.findById(personaRest.getPersCedu()).isPresent()) {
            LOGGER.info("Persona " + personaRest.getPersCedu() + ", ya existe");
            throw new NotFountException("PERSONA_EXISTE", "PERSONA_EXISTE");
        }

        if (funCalculaEdad(personaRest.getPersFena()).getYears() < 18) {
            LOGGER.info("Persona " + personaRest.getPersCedu() + ", ya es Mayor de Edad");
            throw new NotFountException("ES_MENOR_DE_EDAD", "ES_MENOR_DE_EDAD");
        }

        final Persona persona = new Persona();
        persona.setPersCedu(personaRest.getPersCedu());
        persona.setPersNomb(personaRest.getPersNomb());
        persona.setPersApel(personaRest.getPersApel());
        persona.setPersFena(personaRest.getPersFena());


        try {
            LOGGER.info("Intentando registrar la Persona");
            personaRespository.save(persona);
            LOGGER.info("Persona Registrada");
        } catch (final Exception ex) {
            LOGGER.error("INTERNAL_SERVER_ERROR", ex);
            throw new InternalServerErrorException("INTERNAL_SERVER_ERROR", "INTERNAL_SERVER_ERROR");
        }

        return "PERSONA REGISTRADA.!";
    }


    private Period funCalculaEdad(LocalDate dtFecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LOGGER.info("Formato de Fecha := " + formatter);
        LocalDate localDate = LocalDate.parse(String.valueOf(dtFecha), formatter);
        LOGGER.info("Fecha := " + localDate);

        return Period.between(localDate, ldFechHoy);
    }
}
