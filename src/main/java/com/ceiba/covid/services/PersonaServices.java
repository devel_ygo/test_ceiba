package com.ceiba.covid.services;

import com.ceiba.covid.entities.Persona;
import com.ceiba.covid.exceptions.ApiException;
import com.ceiba.covid.json.PersonaRest;

import java.util.List;

public interface PersonaServices {

    List<PersonaRest> getPersonas() throws ApiException;

    PersonaRest getPersona(String strPersonaId) throws ApiException;

    String crearPersona(PersonaRest personaRest) throws ApiException;
}
