package com.ceiba.covid.repositories;

import com.ceiba.covid.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRespository extends JpaRepository<Persona, String> {

}
