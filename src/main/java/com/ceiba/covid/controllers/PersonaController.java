package com.ceiba.covid.controllers;

import com.ceiba.covid.exceptions.ApiException;
import com.ceiba.covid.json.PersonaRest;
import com.ceiba.covid.responses.Response;
import com.ceiba.covid.services.PersonaServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class PersonaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaController.class);

    @Autowired
    private PersonaServices personaServices;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "personas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PersonaRest> getpersonas() throws ApiException {
        return personaServices.getPersonas();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "persona/{personaId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<PersonaRest> getPersona(@PathVariable(name = "personaId") String strPersonaId) throws ApiException {
        LOGGER.info("Ingreso al metodo getPersona con parámetro := " + strPersonaId);

        PersonaRest personaRest = personaServices.getPersona(strPersonaId);
        if (personaRest == null) {
            return new Response<>("400", String.valueOf(HttpStatus.BAD_REQUEST), "NO EXISTE", personaRest);
        }
        return new Response<>("Succes", String.valueOf(HttpStatus.OK), "OK", personaRest);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "persona", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<String> crearPersona(@RequestBody @Valid PersonaRest personaRest)
            throws ApiException {
        LOGGER.info("Ingreso al metodo crearPersona");
        return new Response<>("Succes", String.valueOf(HttpStatus.OK), "OK", personaServices.crearPersona(personaRest));
    }
}
