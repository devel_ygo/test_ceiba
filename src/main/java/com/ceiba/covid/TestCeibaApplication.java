package com.ceiba.covid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCeibaApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestCeibaApplication.class, args);
    }
}
