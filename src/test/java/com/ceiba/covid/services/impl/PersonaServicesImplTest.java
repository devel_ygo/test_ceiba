package com.ceiba.covid.services.impl;

import com.ceiba.covid.entities.Persona;
import com.ceiba.covid.exceptions.ApiException;
import com.ceiba.covid.json.PersonaRest;
import com.ceiba.covid.repositories.PersonaRespository;
import com.ceiba.covid.responses.Response;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.*;

public class PersonaServicesImplTest {

    private static String STR_RESULTADO_MENSAJE = "PERSONA REGISTRADA.!";
    private static String STR_ESPERADO_MENSAJE = null;
    private static final String PERSONA_ID_NULL = null;
    private static final String PERSONA_ID = "123";
    private static final String PERSONA_NOMBRE = "Yefri";
    private static final String PERSONA_APELLIDO = "Gomez";
    private static final LocalDate PERSONA_FECHA_NACIMIENTO = LocalDate.now();
    private static final LocalDate PERSONA_FECHA_NACIMIENTO_OK = LocalDate.parse("1995-06-10");

    private static final Persona PERSONA = new Persona();
    private static PersonaRest PERSONAREST = new PersonaRest();
    private static PersonaRest PERSONAREST_NULL = null;

    private static Response<PersonaRest> RESPONSE = null;
    private static Response<String> RESPONSE_STRING = null;
    private static Response RESPONSE_OK = new Response("Succes", String.valueOf(HttpStatus.OK), "OK");
    private static Response RESPONSE_NOT_FOUND = new Response("SNOT-404-1", String.valueOf(HttpStatus.NOT_FOUND), "RESTAURANT_NOT_FOUND");

    @InjectMocks
    private PersonaServicesImpl personaServices;

    @Mock
    private PersonaRespository personaRespository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PERSONAREST.setPersCedu(PERSONA_ID);
        PERSONAREST.setPersNomb(PERSONA_NOMBRE);
        PERSONAREST.setPersApel(PERSONA_APELLIDO);
        PERSONAREST.setPersFena(PERSONA_FECHA_NACIMIENTO);

    }

    @Test
    public void getPersonaOKTest() throws ApiException {
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.of(PERSONA));
        PERSONAREST = personaServices.getPersona(PERSONA_ID);
        assertNotNull(PERSONAREST);
    }

    @Test(expected = ApiException.class)
    public void getPersonaParametroNuloTest() throws ApiException {
        Mockito.when(personaRespository.findById(PERSONA_ID_NULL)).thenReturn(Optional.empty());
        PERSONAREST = personaServices.getPersona(PERSONA_ID_NULL);
        fail();
    }

    @Test
    public void getPersonaNoExisteTest() throws ApiException {
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.empty());
        PERSONAREST_NULL = personaServices.getPersona(PERSONA_ID);
        assertNull(PERSONAREST_NULL);
    }

    @Test(expected = ApiException.class)
    public void crearPersonaExisteRegistradaExectionTest() throws ApiException {
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.of(PERSONA));
        STR_ESPERADO_MENSAJE = personaServices.crearPersona(PERSONAREST);
        assertNotEquals(STR_RESULTADO_MENSAJE, STR_ESPERADO_MENSAJE);
        fail();
    }

    @Test(expected = ApiException.class)
    public void crearPersonaMayorEdadExceptionTest() throws ApiException {
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.empty());
        STR_ESPERADO_MENSAJE = personaServices.crearPersona(PERSONAREST);
        assertNotEquals(STR_RESULTADO_MENSAJE, STR_ESPERADO_MENSAJE);
        fail();
    }

    @Test
    public void crearPersonaOKTest() throws ApiException {
        PERSONAREST.setPersFena(PERSONA_FECHA_NACIMIENTO_OK);
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.empty());
        STR_ESPERADO_MENSAJE = personaServices.crearPersona(PERSONAREST);
        assertEquals(STR_RESULTADO_MENSAJE, STR_ESPERADO_MENSAJE);
    }

    @Test (expected = ApiException.class)
    public void crearPersonaExceptionInternalServerTest() throws ApiException {
        PERSONAREST.setPersFena(PERSONA_FECHA_NACIMIENTO_OK);
        Mockito.when(personaRespository.findById(PERSONA_ID)).thenReturn(Optional.empty());
        Mockito.doThrow(Exception.class).when(personaRespository).save(Mockito.any(Persona.class));
        STR_ESPERADO_MENSAJE = personaServices.crearPersona(PERSONAREST);
    }

}