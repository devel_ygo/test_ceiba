package com.ceiba.covid.controllers;

import com.ceiba.covid.exceptions.ApiException;
import com.ceiba.covid.json.PersonaRest;
import com.ceiba.covid.responses.Response;
import com.ceiba.covid.services.PersonaServices;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

public class PersonaControllerTest {

    private static final String STR_RESULTADO_MENSAJE = "PERSONA REGISTRADA.!";
    private static final String PERSONA_ID = "123";
    private static final String PERSONA_NOMBRE = "Yefri";
    private static final String PERSONA_APELLIDO = "Gomez";
    private static final LocalDate PERSONA_FECHA_NACIMIENTO = LocalDate.now();

    private static final PersonaRest PERSONAREST = new PersonaRest();

    private static Response<List<PersonaRest>> LIST_RESPONSE = null;
    private static Response<PersonaRest> RESPONSE = null;

    private static Response<String> RESPONSE_STRING = null;
    private static Response RESPONSE_OK = new Response("Succes", String.valueOf(HttpStatus.OK), "OK");
    private static Response RESPONSE_NOT_FOUND = new Response("SNOT-404-1", String.valueOf(HttpStatus.NOT_FOUND), "RESTAURANT_NOT_FOUND");
    private static Response RESPONSE_BAD_REQUEST = new Response("400", String.valueOf(HttpStatus.BAD_REQUEST), "NO EXISTE");

    @InjectMocks
    private PersonaController personaController;

    @Mock
    private PersonaServices personaServices;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PERSONAREST.setPersCedu(PERSONA_ID);
        PERSONAREST.setPersNomb(PERSONA_NOMBRE);
        PERSONAREST.setPersApel(PERSONA_APELLIDO);
        PERSONAREST.setPersFena(PERSONA_FECHA_NACIMIENTO);

    }

    @Test
    public void getPersonaListTest() throws ApiException {
        Mockito.when(personaServices.getPersonas()).thenReturn(Arrays.asList(PERSONAREST));
        //LIST_RESPONSE = personaController.getpersonas();
        RESPONSE_OK.setData(PERSONAREST);
    }


    @Test
    public void getPersonaOkTest() throws ApiException {
        Mockito.when(personaServices.getPersona(PERSONA_ID)).thenReturn(PERSONAREST);
        RESPONSE = personaController.getPersona(PERSONA_ID);
        RESPONSE_OK.setData(PERSONAREST);
        assertNotNull(RESPONSE);
        assertEquals(RESPONSE.getStatus(), RESPONSE.getStatus());
        assertEquals(RESPONSE.getCode(), RESPONSE.getCode());
        assertEquals(RESPONSE.getMessage(), RESPONSE.getMessage());
        assertEquals(RESPONSE.getData(), RESPONSE.getData());
    }

    @Test
    public void getPersonaNotExistTest() throws ApiException {
        Mockito.when(personaServices.getPersona(PERSONA_ID)).thenReturn(null);
        RESPONSE = personaController.getPersona(PERSONA_ID);
        assertNotNull(RESPONSE);
        assertEquals(RESPONSE_BAD_REQUEST.getStatus(), RESPONSE.getStatus());
        assertEquals(RESPONSE_BAD_REQUEST.getCode(), RESPONSE.getCode());
        assertEquals(RESPONSE_BAD_REQUEST.getMessage(), RESPONSE.getMessage());
    }


    @Test
    public void crearPersonaOKTest() throws ApiException {
        Mockito.when(personaServices.crearPersona(PERSONAREST)).thenReturn(STR_RESULTADO_MENSAJE);
        RESPONSE_STRING = personaController.crearPersona(PERSONAREST);
        RESPONSE_OK.setData(STR_RESULTADO_MENSAJE);
    }
}































